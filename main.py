from typing import List
from copy import deepcopy
import argparse


class GlyphLevelFactorFormula:
    def __init__(self, resource_count):
        self.name = 'Abstract Class'
        self.resource_count = resource_count
        self.multiplier_weighted_base = 0
        self.exponent_weighted_base = 0

    def get_name(self) -> str:
        return self.name

    def evaluate(self, weight) -> float:
        multiplier = self._get_multiplier(weight)
        exponent = self._get_exponent(weight)
        return multiplier * self.resource_count ** exponent

    def _get_multiplier(self, weight) -> float:
        # https://antimatter-dimensions.fandom.com/wiki/Glyphs
        return ((self.multiplier_weighted_base * 5) ** ((4 * weight) ** (1 / 3))) / 5

    def _get_exponent(self, weight) -> float:
        # https://antimatter-dimensions.fandom.com/wiki/Glyphs
        return (self.exponent_weighted_base * (4 * weight)) ** (1 / 3)


class EternityPointGlyphLevelFactorFormula(GlyphLevelFactorFormula):
    def __init__(self, resource_count):
        super().__init__(resource_count)
        self.name = 'Eternity Points'
        # https://www.wolframalpha.com/input?i=solve+%28%28b*5%29%5E%28%284*w%29%5E%281%2F3%29%29%29%2F5+%3D+0.016+where+w+%3D+25
        self.multiplier_weighted_base = 0.11607
        # https://www.wolframalpha.com/input?i=solve+%28b*%284*w%29%29%5E%281%2F3%29+%3D+0.5+where+w+%3D+25
        self.exponent_weighted_base = 0.00125


class ReplicantiGlyphLevelFactorFormula(GlyphLevelFactorFormula):
    def __init__(self, resource_count, glyph_bonus):
        super().__init__(resource_count)
        self.name = 'Replicanti'
        # https://www.wolframalpha.com/input?i=solve+%28%28b*5%29%5E%28%284*w%29%5E%281%2F3%29%29%29%2F5+%3D+0.025+where+w+%3D+25
        self.multiplier_weighted_base = 0.12778
        # https://www.wolframalpha.com/input?i=solve+%28b*%284*w%29%29%5E%281%2F3%29+%3D+0.4+where+w+%3D+25
        # https://www.wolframalpha.com/input?i=solve+for+b+%28b*%284*w%29%29%5E%281%2F3%29+%3D+0.4+%2B+g+where+w+%3D+25
        g = glyph_bonus
        self.exponent_weighted_base = (125 * g ** 3 + 150 * g ** 2 + 60 * g + 8) / 12_500


class DilatedTimeGlyphLevelFactorFormula(GlyphLevelFactorFormula):
    def __init__(self, resource_count):
        super().__init__(resource_count)
        self.name = 'Dilated Time'
        self.multiplier_weighted_base = 0.12778
        self.exponent_weighted_base = 0.02197


class EternitiesGlyphLevelFactorFormula(GlyphLevelFactorFormula):
    def __init__(self, resource_count):
        super().__init__(resource_count)
        self.name = 'Eternities'
        self.multiplier_weighted_base = 0.23818
        self.exponent_weighted_base = 0.00125


class WeightedGlyphLevelFactor:
    def __init__(self, glyph_level_factor_formula: GlyphLevelFactorFormula):
        self._glyph_level_factor_formula = glyph_level_factor_formula
        self._weight = 1

    def __str__(self):
        return f'{self._glyph_level_factor_formula.get_name()}: {self._weight}'

    def increment_weight(self):
        self._weight += 1

    def get_weight(self) -> int:
        return self._weight

    def evaluate(self) -> float:
        return self._glyph_level_factor_formula.evaluate(self._weight)


class GlyphLevelFactors:
    def __init__(self):
        self._factors: List[WeightedGlyphLevelFactor] = []

    def __str__(self):
        return ', '.join(str(factor) for factor in self._factors)

    def add_factor(self, factor: WeightedGlyphLevelFactor):
        self._factors.append(factor)

    def add_factors(self, factors: List[WeightedGlyphLevelFactor]):
        self._factors.extend(factors)

    def sum_of_weights(self) -> int:
        return sum(f.get_weight() for f in self._factors)

    def evaluate_level(self) -> float:
        level = 1
        for factor in self._factors:
            level *= factor.evaluate()
        return level

    def get_increment_candidates(self) -> List['GlyphLevelFactors']:
        increments = []
        for factor_index, _ in enumerate(self._factors):
            factors_with_index_incremented = deepcopy(self._factors)
            factors_with_index_incremented[factor_index].increment_weight()
            incremented_glyph_level_factors = GlyphLevelFactors()
            incremented_glyph_level_factors.add_factors(factors_with_index_incremented)
            increments.append(incremented_glyph_level_factors)
        return increments


class Save:
    def __init__(self, ep_exponent, replicanti_exponent, replicanti_bonus, dt_exponent, eternities_exponent):
        self.ep_exponent = ep_exponent
        self.replicanti_exponent = replicanti_exponent
        self.replicanti_bonus = replicanti_bonus
        self.dt_exponent = dt_exponent
        self.eternities_exponent = eternities_exponent


def main(save: Save):
    factors = GlyphLevelFactors()
    factors.add_factor(WeightedGlyphLevelFactor(EternityPointGlyphLevelFactorFormula(save.ep_exponent)))
    factors.add_factor(WeightedGlyphLevelFactor(ReplicantiGlyphLevelFactorFormula(
        save.replicanti_exponent, save.replicanti_bonus)))
    factors.add_factor(WeightedGlyphLevelFactor(DilatedTimeGlyphLevelFactorFormula(save.dt_exponent)))
    factors.add_factor(WeightedGlyphLevelFactor(EternitiesGlyphLevelFactorFormula(save.eternities_exponent)))

    while factors.sum_of_weights() < 100:
        candidate_factors = factors.get_increment_candidates()
        candidate_levels = []
        for candidate_factor in candidate_factors:
            candidate_levels.append((candidate_factor.evaluate_level(), candidate_factor))
        factors = max(candidate_levels, key=lambda c: c[0])[1]

    print(factors)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Iteratively computes the optimal glyph level factors for Antimatter '
                                                 'Dimensions')
    parser.add_argument('-e', metavar='EP_EXPONENT', type=int, default=0, help='Eternity Point Exponent')
    parser.add_argument('-r', metavar='REPLICANTI_EXPONENT', type=int, default=0, help='Replicanti Amount Exponent')
    parser.add_argument('--rb', metavar='REPLICANTI_BONUS', type=float, default=0, help='Bonus Replicanti Factor '
                                                                                        'Exponent from glyphs')
    parser.add_argument('-d', metavar='DT_EXPONENT', type=int, default=0, help='Dilated Time Exponent')
    parser.add_argument('-t', metavar='ETERNITIES_EXPONENT', type=int, default=0, help='Eternities Exponent')
    args = parser.parse_args()

    main(Save(args.e, args.r, args.rb, args.d, args.t))
