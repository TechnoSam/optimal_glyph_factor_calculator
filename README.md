# Optimal Glyph Factor Calculator

Iteratively computes the optimal glyph level factors for Antimatter Dimensions. This works because the slope of each
factor curve is always positive and always decreasing. Starting from one point in each factor, there will always be
a factor which provides the most marginal levels per point. You'll never get a better deal again, so it's always
the optimal choice. This can be repeated until all 100 points are assigned.

## Usage

```text
usage: main.py [-h] [-e EP_EXPONENT] [-r REPLICANTI_EXPONENT] [--rb REPLICANTI_BONUS] [-d DT_EXPONENT] [-t ETERNITIES_EXPONENT]

Iteratively computes the optimal glyph level factors for Antimatter Dimensions

options:
  -h, --help            show this help message and exit
  -e EP_EXPONENT        Eternity Point Exponent
  -r REPLICANTI_EXPONENT
                        Replicanti Amount Exponent
  --rb REPLICANTI_BONUS
                        Bonus Replicanti Factor Exponent from glyphs
  -d DT_EXPONENT        Dilated Time Exponent
  -t ETERNITIES_EXPONENT
                        Eternities Exponent
```

## Example

```text
$ python main.py -e 27343 -r 170400 --rb 0.112 -d 171 -t 34
Eternity Points: 16, Replicanti: 31, Dilated Time: 37, Eternities: 16
```